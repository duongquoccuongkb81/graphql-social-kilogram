import mongoose from 'mongoose';
import Comment from './Comment';
import Favorite from "./Favorite";
import fileSchema from './File';



const postSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  published: {
    type: Boolean,
    default: true
  },
  favoritesCount: {
    type: Number,
    default: 0
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users'
  },
  image: fileSchema
}, {
  timestamps: true
});

postSchema.virtual('comments', {
  ref: 'Comments',
  localField: '_id',
  foreignField: 'post'
});

postSchema.virtual('countComments', {
  ref: 'Comments',
  localField: '_id',
  foreignField: 'post',
  count: true
});

postSchema.virtual('favorites', {
  ref: 'Favorites',
  localField: '_id',
  foreignField: 'post'
});

postSchema.virtual('countFavorites', {
  ref: 'Favorites',
  localField: '_id',
  foreignField: 'post',
  count: true
});

postSchema.pre('remove', async function (next) {
  const post = this;
  await Favorite.deleteMany({ post: post._id });
  await Comment.deleteMany({ post: post._id });
  next();
});

postSchema.methods.updateFavoriteCount = async function () {
  const post = this;
  await post.populate('countFavorites').execPopulate();
  post.favoritesCount = post.countFavorites;
  await post.save();
}

const Post = mongoose.model('Posts', postSchema);

export default Post;