import { GraphQLServer, PubSub } from 'graphql-yoga';
import express from 'express';
import { resolvers } from './resolvers/index';
import models from './models/index';
import { authPermissions } from './middleware/auth';
import permissions from './permissions/index';
import morgan from "morgan";
import bodyParser from 'body-parser';
import path from 'path';

import('./db/mongoose');

const pubsub = new PubSub();

const server = new GraphQLServer({
  typeDefs: './src/schema/schema.graphql',
  resolvers,
  context(req, connection) {
    return {
      models,
      pubsub,
      req,
      connection
    };
  },
  middlewares: [authPermissions, permissions]
});

server.express.use('/images' ,express.static(path.join(__dirname, '../uploads')));
server.use(bodyParser.urlencoded({ extended: false }));

morgan.token('graphql-query', (req) => {
  const { query, variables, operationName } = req.body;
  if (operationName === 'IntrospectionQuery' || operationName === undefined) {
    return;
  }
  return `GRAPHQL: \nOperation Name: ${operationName} \nQuery: ${query} \nVariables: ${JSON.stringify(variables)}`;
});

server.use(bodyParser.json());
server.use(morgan(':graphql-query'));
server.use(morgan('tiny'));

server.express.get('/about', (req, res) => {
  res.send('<h1>Hello Cường Mập</h1>');
});

export default server;
