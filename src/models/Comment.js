import mongoose from 'mongoose';
import Filter from 'bad-words';

const filter = new Filter();
const commentSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
    trim: true,
    validate(value) {
      if (filter.isProfane(value)) {
        throw new Error('Comment must not contain bad words');
      }
    }
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users'
  },
  post: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Posts'
  }
}, {
  timestamps: true
});

const Comment = mongoose.model('Comments', commentSchema);

export default Comment;