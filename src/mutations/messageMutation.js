import debug from 'debug';
const logger = debug('app:messageMutation');

const messageMutation = {
  async sendMessage(parent, { data }, { models, user, pubsub }, info) {
    const userExist = await models.User.findById(data.userId);
    if (!userExist) {
      throw new Error('Receiver not found');
    }
    const ids = [user._id, data.userId];
    const conversationExist = await models.Conversation.findOne({ participants: { $all: ids } });
    if (!conversationExist) {
      const conversation = new models.Conversation({
        participants: ids
      });
      await conversation.save();
      const message = new models.Message({
        conversation_id: conversation._id,
        sender: user._id,
        text: data.text
      });
      await message.save();
      pubsub.publish(`GlobalConversationChanel ${data.userId}`, {
        conversation: {
          mutation: 'CREATED',
          data: conversation,
          message: message
        }
      });
      return message;
    }
    const message = new models.Message({
      conversation_id: conversationExist._id,
      sender: user._id,
      text: data.text
    });
    await message.save();
    await conversationExist.updateTime();
    pubsub.publish(`GlobalConversationChanel ${data.userId}`, {
      conversation: {
        mutation: 'CREATED',
        data: conversationExist,
        message: message
      }
    });
    pubsub.publish(`ConversationChanel-${message.conversation_id}`, {
      message: {
        mutation: 'CREATED',
        data: message
      }
    });
    // })
    return message;
  },
  async joinTheConversation(parent, { userId }, { models, user }, info) {
    const userExist = await models.User.findById(userId);
    if (!userExist) {
      throw new Error('Receiver not found');
    }
    console.log('abc');
    const ids = [user._id, userId];
    const conversationExist = await models.Conversation.findOne({ participants: { $all: ids } });
    if (!conversationExist) {
      const conversation = new models.Conversation({
        participants: ids
      });
      await conversation.save();
      return conversation;
    }
    return conversationExist;
  }
}

export default messageMutation;