const commentMutation = {
  async createComment(parent, { data }, { models, user, pubsub }, info) {
    const postExists = await models.Post.findOne({ _id: data.postId, published: true});
    if (!postExists) {
      throw new Error('Unable to find post');
    }
    const comment = new models.Comment({ text: data.text, author: user._id, post: data.postId });
    await comment.save();
    pubsub.publish(`commentChanel with post ${data.postId}`, {
      comment: {
        mutation: 'CREATED',
        data: comment
      }
    });
    return comment;
  },
  async updateComment(parent, { id, data }, { models, user, pubsub }, info) {
    const comment = await models.Comment.findById(id);
    if (!comment) {
      throw new Error('Unable to find comment');
    }

    if (comment.author.toString() !== user._id.toString()) {
      throw new Error('Unable to update comment');
    }

    for (const [key, value] of Object.entries(data)) {
      comment[key] = value;
    }
    await comment.save();
    pubsub.publish(`commentChanel with post ${comment.post}`, {
      comment: {
        mutation: 'UPDATED',
        data: comment
      }
    });
    return comment;
  },
  async deleteComment(parent, { id }, { models, user, pubsub }, info) {
    const comment = await models.Comment.findOne({ _id: id, author: user._id });
    if (!comment) {
      throw new Error('Unable to delete comment!');
    }
    await comment.remove();
    pubsub.publish(`commentChanel with post ${comment.post}`, {
      comment: {
        mutation: 'DELETED',
        data: comment
      }
    });
    return comment;
  }
}

export  { commentMutation as default };