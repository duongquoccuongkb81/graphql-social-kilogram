import mongoose from 'mongoose';

const conversationSchema = new mongoose.Schema({
  participants: {
    type: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    }],
    validate(value) {
      if (value.length !== 2) {
        throw new Error('Conversation must has two user!');
      }
    }
  }
}, {
  timestamps: true
});

conversationSchema.pre('save', async function(next) {
  const conversation = this;
  conversation.updatedAt = Date.now();
  next();
});

conversationSchema.virtual('countMessages', {
  ref: 'Messages',
  localField: '_id',
  foreignField: 'conversation_id',
  count: true
});

conversationSchema.virtual('messages', {
  ref: 'Messages',
  localField: '_id',
  foreignField: 'conversation_id',
});

conversationSchema.methods.visible = async function() {
  const conversation = this;
  await conversation.populate('countMessages').execPopulate();
  if (conversation.countMessages > 0) {
    return true;
  }
  return false;
}

conversationSchema.methods.updateTime = async function() {
  const conversation = this;
  conversation.updatedAt = Date.now();
  await conversation.save();
}

const Conversation = mongoose.model('Conversations', conversationSchema);

export default Conversation;