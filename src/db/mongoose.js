import mongoose from 'mongoose';
import debug from 'debug';

const dbLogger = debug('app:db');

mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});

mongoose.connection.once('open', () => {
  dbLogger('Connected to database!');
});
