import { formatDateComment } from '../utils/formatDate';


const Comment = {
  async author(parent, args, { models }, info) {
    const user = models.User.findById(parent.author);
    return user;
  },
  async post(parent, args, { models }, info) {
    const post = models.Post.findById(parent.post);
    return post;
  },
  createdAt(parent, args, info, ctx) {
    if (!args.format) {
      return parent.createdAt
    }
    const timeFormated = args.format === 'CLIENT' ? formatDateComment(parent.createdAt) : parent.createdAt;
    return timeFormated;
  }
}

export { Comment as default };