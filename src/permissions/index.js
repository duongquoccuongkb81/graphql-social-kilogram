import { rule, shield, and, or, not } from 'graphql-shield';
import debug from 'debug';

const logger = debug('app:PermissionMiddleware');

const isAuthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    if (ctx.user !== null) {
      return true;
    } else {
      return new Error('Authenticated Fail!');
    }
  }
);

const isAuthenticatedUpload = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    if (ctx.user !== null) {
      return true;
    } else {
      if (args.image || args.data.image) {
        const upload = args.image ? args.image : args.data.image;
        logger(upload);
        const { createReadStream } = await upload;
        const stream = createReadStream();
        for await (const uploadChunk of stream) {
        }
        logger(stream.isPaused());
        stream.pause();
        logger(stream.isPaused());
        //stream.emit('error', new Error('Stop uploading'));
        stream.destroy();
      }
      return new Error('Authenticated Fail!');
      //return false;
    }
  }
);

const isNotAuthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    return ctx.user === null
  }
);

const isOwner = rule({ cache: 'strict' })(
  async (parent, args, ctx, info) => {
    return parent.id === ctx.user.id;
  }
);

const permissions = shield({
  Query: {
    me: isAuthenticated,
    myPosts: isAuthenticated,
    post: or(isNotAuthenticated, isAuthenticated),
    myConversations: isAuthenticated,
    conversation: isAuthenticated,
    followingPosts: isAuthenticated,
    checkLogin: isAuthenticated,
    myFavorites: isAuthenticated
  },
  Mutation: {
    uploadAvatar: isAuthenticatedUpload,
    deleteUser: isAuthenticated,
    updateUser: isAuthenticated,
    createPost: isAuthenticatedUpload,
    updatePost: isAuthenticated,
    deletePost: isAuthenticated,
    createComment: isAuthenticated,
    deleteComment: isAuthenticated,
    updateComment: isAuthenticated,
    favoritePost: isAuthenticated,
    unFavoritePost: isAuthenticated,
    follow: isAuthenticated,
    unfollow: isAuthenticated,
    logout: isAuthenticated,
    logoutAll: isAuthenticated,
    sendMessage: isAuthenticated,
    joinTheConversation: isAuthenticated
  },
  User: {
    email: or(isNotAuthenticated, isAuthenticated),
    posts: or(isNotAuthenticated, isAuthenticated),
    favorites: or(isNotAuthenticated, isAuthenticated),
    followed: or(isNotAuthenticated, isAuthenticated)
  },
  Post: {
    favorited: or(isNotAuthenticated, isAuthenticated)
  },
  Subscription: isAuthenticated,
  Conversation: {
    withUser: isAuthenticated
  }
}, { allowExternalErrors: true }); // Custom Error :v

export { permissions as default };