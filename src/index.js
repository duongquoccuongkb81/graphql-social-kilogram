import '@babel/polyfill';
import debug from 'debug';
import server from './server';

const port = process.env.PORT || 3020;

const serverLogger = debug('app:server');

server.start({ 
  port,
  endpoint: '/graphql',
  playground: '/playground',
  subscriptions: {
    path: '/graphql',
    keepAlive: 1000
  }
}, () => {
  serverLogger(`The server is up on ${port}!`);
});
