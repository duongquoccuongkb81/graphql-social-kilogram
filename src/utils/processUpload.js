import debug from 'debug';
import mongoose from 'mongoose';
import cloudinary from '../cloudinary/cloudinary';
import { createWriteStream } from 'fs';

const logger = debug('app:processUpload');

const cloudinaryUpload = (stream, id) => {
  return new Promise((resolve, reject) => {
    const upload_stream = cloudinary.v2.uploader.upload_stream({
      public_id: id,
      folder: 'images'
    }, (error, result) => {
      if(error) {
        reject(error);
      }
      resolve(result);
    });
    stream.pipe(upload_stream);
  });
}

const processDestroy = imageId => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.destroy(imageId, function(error,result) {
      if (error) {
        reject(error);
      }
      resolve(result);
    });
  });
}

const processUpload = async upload => {
  const { createReadStream, filename, mimetype, encoding } = await upload;
  if(mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
    throw new Error('Unsupported file type, please upload an image!');
  }

  const uploadStream = createReadStream();
  let byteLength = 0;
  for await (const uploadChunk of uploadStream) {
    byteLength += Buffer.from(uploadChunk).byteLength;

    if (byteLength > 20000000) {
      uploadStream.destroy();
      throw new Error(`Can't upload file bigger than 20MB`);
    }
  }

  const stream = createReadStream();
  
  const fileID = new mongoose.Types.ObjectId();

  try {
    const result = await cloudinaryUpload(stream, fileID);
    
    return {
      _id: fileID,
      path: result.url,
      filename: filename,
      mimetype: mimetype,
      encoding: encoding
    }
  } catch (error) {
    throw new Error(error);
  }
}

export { processUpload, processDestroy };