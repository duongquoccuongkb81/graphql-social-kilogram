const favoriteMutation = {
  async favoritePost(parent, { postId }, { models, user }, info) {
    const postExist = await models.Post.findOne({ _id: postId, published: true });
    const postOwner = await models.Post.findOne({ _id: postId, author: user._id });
    if (!postExist && !postOwner) {
      throw new Error('Post not found!');
    }
    const isFavorited = await models.Favorite.findOne({ post: postId, likedBy: user._id });
    if (isFavorited) {
      throw new Error('This post is favorited');
    }
    const favorite = new models.Favorite({ post: postId, likedBy: user._id });
    await favorite.save();
    return favorite;
  },
  async unFavoritePost(parent, { postId }, { models, user }, info) {
    const favorite = await models.Favorite.findOne({ post: postId, likedBy: user._id });
    if (!favorite) {
      throw new Error('Unable to unFavorite post');
    }
    await favorite.remove();
    return favorite;
  }
}

export default favoriteMutation;