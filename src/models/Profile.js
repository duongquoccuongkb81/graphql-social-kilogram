import mongoose from 'mongoose';
import fileSchema from './File';


const profileSchema = new mongoose.Schema({
  bio: {
    type: String
  },
  name: {
    type: String,
    required: true
  },
  avatar: fileSchema
});

export default profileSchema;