// import debug from 'debug';
import { formatDateConversation } from '../utils/formatDate';

// const logger = debug('app:ConversationResolver');

const Conversation = {
  async withUser(parent, args, { models, user }, info) {
    const withUserId = parent.participants.find(id => id.toString() !== user._id.toString());
    const withUser = await models.User.findById(withUserId);
    return withUser;
  },
  async messages(parent, args, { models }, info) {
    const match = {};
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      match._id = {
        $lt: args.after
      }
      await parent.populate({
        match,
        path: 'messages',
        options: {
          limit,
          skip,
          sort: sortOption
        }
      }).execPopulate();

      return parent.messages;
    }
    
    await parent.populate({
      match,
      path: 'messages',
      options: {
        limit,
        skip,
        sort: sortOption
      }
    }).execPopulate();

    return parent.messages;
  },
  visible(parent, args, { models }, info) {
    return parent.visible();
  },
  createdAt(parent, args, info, ctx) {
    if (!args.format) {
      return parent.createdAt
    }
    const timeFormated = args.format === 'CLIENT' ? formatDateConversation(parent.createdAt) : parent.createdAt;
    return timeFormated;
  },
  updatedAt(parent, args, info, ctx) {
    if (!args.format) {
      return parent.createdAt
    }
    const timeFormated = args.format === 'CLIENT' ? formatDateConversation(parent.updatedAt) : parent.updatedAt;
    return timeFormated;
  }
}

export default Conversation;