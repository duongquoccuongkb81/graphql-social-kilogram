const Subscription = {
  post: {
    subscribe(parent, args, { user, pubsub }, info) {
      return pubsub.asyncIterator(`postChanel ${user._id}`);
    }
  },
  comment: {
    subscribe(parent, { postId }, { pubsub }, info) {
      return pubsub.asyncIterator(`commentChanel with post ${postId}`);
    }
  },
  message: {
    async subscribe(parent, { conversationId }, { pubsub, user, models }, info) {
      const conversation = await models.Conversation.findOne({ _id: conversationId, 'participants': user._id });
      if (!conversation) {
        throw new Error(`You do not have access to this conversation`);
      }
      return pubsub.asyncIterator(`ConversationChanel-${conversationId}`);
    }
  },
  conversation: {
    subscribe(parent, args, { user, pubsub }, info) {
      return pubsub.asyncIterator(`GlobalConversationChanel ${user._id}`);
    }
  }
}

export default Subscription;