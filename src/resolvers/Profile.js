import cloudinary from '../cloudinary/cloudinary';
//import debug from 'debug';

//const logger = debug('app:ProfileResolver');
const Profile = {
  avatar(parent, args, { models }, info) {
    if(!parent.avatar) {
      return null;
    }
    if (!args.transform) {
      return parent.avatar;
    }
    const transform = args.transform;
    if (transform.gravity) {
      transform.gravity = transform.gravity.replace('_', ':');
    }
    const path = cloudinary.v2.url(`images/${parent.avatar.id}`, {
      ...transform,
      quality: 100
    });
    
    const image = {
      id: parent.avatar.id,
      path,
      filename: parent.avatar.filename,
      mimetype: parent.avatar.mimetype,
      encoding: parent.avatar.encoding
    }
    
    return image;
  }
}

export default Profile;