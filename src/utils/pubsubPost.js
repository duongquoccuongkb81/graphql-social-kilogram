const pubsubPost = async ({ userId, post, mutation, pubsub, models }) => {
  const follower = await models.Follower.findOne({ userId });
  const myFollowers = await models.User.find({ _id: { $in: follower.followers } });
  myFollowers.forEach(user => {
    pubsub.publish(`postChanel ${user._id}`, {
      post: {
        mutation,
        data: post
      }
    });
  })
}

export default pubsubPost;