import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Post from './Post';
import Comment from './Comment';
import Favorite from './Favorite'
import Following from './Following';
import Follower from './Follower';
import Conversation from './Conversation';
import Message from './Message';
import profileSchema from './Profile';

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    validate(value) {
      const splitValue = value.split(' ');
      if (splitValue.length > 1) {
        throw new Error('Name must not contrain white space');
      }
    }
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Email is invalid');
      }
    }
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 8,
    validate(value) {
      if (value.toLowerCase().includes('password')) {
        throw new Error(`Password must not contain 'password'`);
      }
    }
  },
  age: {
    type: Number,
    default: 0,
    validate(value) {
      if (value < 0) {
        throw new Error('Age must be a postive number');
      }
    }
  },
  gender: {
    type: String,
    enum: ['Male', 'Female']
  },
  profile: profileSchema,
  tokens: [{
    token: {
      type: String,
      required: true
    }
  }]
}, {
  timestamps: true
});

userSchema.index({ email: 1 }, { unique: true });

userSchema.virtual('posts', {
  ref: 'Posts',
  localField: '_id',
  foreignField: 'author'
});

userSchema.virtual('comments', {
  ref: 'Comments',
  localField: '_id',
  foreignField: 'author'
});

userSchema.virtual('favorites', {
  ref: 'Favorties',
  localField: '_id',
  foreignField: 'likedBy'
});

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();
  delete userObject.password;
  delete userObject.tokens;
  return userObject;
}

userSchema.pre('save', async function (next) {
  const user = this;

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 10);
  }
  //Lưu lại state new trước khi save để dùng ở middleware post, vì sau khi save user.isNew = false
  user.$locals.wasNew = user.isNew;  
  next();
});

userSchema.post('save', async function (doc) {
  const user = this;
  if (user.$locals.wasNew) {
    const follower = new Follower({ userId: user._id });
    const following = new Following({ userId: user._id });
    await follower.save();
    await following.save();
  }
});

userSchema.pre('remove', async function (next) {
  const user = this;

  await user.populate('posts').execPopulate();
  const postIds = user.posts.map(post => post._id);
  if (postIds.length > 0) {
    await Favorite.deleteMany({ post: { $in: postIds } });
    await Comment.deleteMany({ post: { $in: postIds } });
  }
  await Favorite.deleteMany({ likedBy: user._id });
  await Comment.deleteMany({ author: user._id });
  await Post.deleteMany({ author: user._id });
  await Follower.deleteOne({ userId: user._id });
  await Following.deleteOne({ userId: user._id });
  const conversations = await Conversation.find({ 'participants': user._id });
  const conversationIds = conversations.map(el => el._id);
  if (conversationIds.length > 0) {
    await Message.deleteMany({ conversation_id: { $in: conversationIds } });
  }
  await Conversation.deleteMany({ 'participants': user._id });

  next();
});

userSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Unable to login');
  }

  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new Error('Unable to login');
  }

  return user;
}

userSchema.methods.generateAuthToken = async function() {
  const user = this;
  const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '60d' });
  user.tokens.push({ token });
  await user.save();
  return token;
}

userSchema.methods.isFavorite = async function(postId) {
  const user = this;
  const isFavorited = await Favorite.findOne({ post: postId, likedBy: user._id });
  return isFavorited ? true : false;
}


const User = mongoose.model('Users', userSchema);

export default User;