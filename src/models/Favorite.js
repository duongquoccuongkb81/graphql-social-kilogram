import mongoose from 'mongoose';

const favoriteSchema = new mongoose.Schema({
  post: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Posts',
  },
  likedBy: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
  }
}, {
  timestamps: true,
});

// unique multiple properties required drop database
favoriteSchema.index({ post: 1, likedBy: 1 }, { unique: true });

const Favorite = mongoose.model('Favorites', favoriteSchema);

export default Favorite;