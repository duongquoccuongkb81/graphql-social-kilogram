const Message = {
  async sender(parent, args, { models }, info) {
    const user = await models.User.findById(parent.sender);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  },
  async conversation(parent, args, { models }, info) {
    const conversation = await models.Conversation.findById(parent.conversation_id);
    if (!conversation) {
      throw new Error('Conversation not found!');
    }
    return conversation;
  }
}

export { Message as default }