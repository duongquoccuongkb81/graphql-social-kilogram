import jwt from 'jsonwebtoken';

const auth = async (resolve, parent, args, ctx, info) => {
  // before resolve
  const header = ctx.req.request
    ? ctx.req.request.headers.authorization
    : ctx.req.connection.context.Authorization; // Cái này là dùng khi subscription

  if (!header) {
    ctx.user = null;
    ctx.token = null;
    return resolve(parent, args, ctx, info);
  }
  const token = header.replace('Bearer ', '');
  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  const user = await ctx.models.User.findOne({ _id: decoded.userId, 'tokens.token': token });

  if (user) {
    ctx.user = user;
    ctx.token = token;
  } else {
    ctx.user = null;
    ctx.token = null;
  }
  const result = await resolve(parent, args, ctx, info);
  // After resolve
  return result;
};

// Phần quyền dể middleware chạy quan trọng lắm nha =))
export const authPermissions = {
  Query: {
    me: auth,
    myPosts: auth,
    post: auth,
    myConversations: auth,
    conversation: auth,
    followingPosts: auth,
    checkLogin: auth,
    myFavorites: auth
  },
  Mutation: {
    uploadAvatar: auth,
    deleteUser: auth,
    updateUser: auth,
    createPost: auth,
    updatePost: auth,
    deletePost: auth,
    createComment: auth,
    deleteComment: auth,
    updateComment: auth,
    favoritePost: auth,
    unFavoritePost: auth,
    follow: auth,
    unfollow: auth,
    logout: auth,
    logoutAll: auth,
    sendMessage: auth,
    joinTheConversation: auth
  },
  User: {
    email: auth,
    posts: auth,
    favorites: auth,
    followed: auth
  },
  Post: {
    favorited: auth
  },
  Subscription: auth,
  Conversation: {
    withUser: auth
  }
};
