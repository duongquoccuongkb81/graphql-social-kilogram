import Query from './Query';
import User from './User';
import Post from './Post';
import Comment from './Comment';
import Favorite from './Favorite';
import Date from './Date';
import Mutation from './Mutation';
import Subscription from './Subscription';
import Message from './Message';
import Conversation from './Conversation';
import Profile from './Profile';

const resolvers = {
  Query,
  Mutation,
  User,
  Post,
  Comment,
  Favorite,
  Date,
  Subscription,
  Message,
  Conversation,
  Profile
}


export { resolvers }; 