import { formatDateConversation } from '../utils/formatDate';

const Favorite = {
  async post(parent, args, { models }, info) {
    const post = await models.Post.findById(parent.post);
    return post;
    
  },
  async likedBy(parent, args, { models }, info) {
    const user = await models.User.findById(parent.likedBy);
    return user;
  },
  createdAt(parent, args, info, ctx) {
    if (!args.format) {
      return parent.createdAt
    }
    const timeFormated = args.format === 'CLIENT' ? formatDateConversation(parent.createdAt) : parent.createdAt;
    return timeFormated;
  }
}

export default Favorite;