import debug from 'debug';

import pubsubPost from '../utils/pubsubPost';
import {processUpload, processDestroy} from '../utils/processUpload';

const logger = debug('app:postMutation');


const postMutation = {
  async createPost(parent, { data }, { models, user, pubsub }, info) {
    const post = new models.Post({
      title: data.title,
      published: data.published,
      author: user._id 
    });
    const image = await processUpload(data.image);
    post.image = image;
    await post.save();
    if (data.published) {
      await pubsubPost({
        userId: user._id,
        post,
        mutation: 'CREATED',
        pubsub,
        models
      });
    }
    return post;
  },
  async updatePost(parent, args, { models, user, pubsub }, info) {
    const post = await models.Post.findOne({ _id: args.id, author: user._id });
    if (!post) {
      throw new Error('Unable to update post!');
    }
    const prePublished = post.published;
    for (const [key, value] of Object.entries(args.data)) {
      post[key] = value
    }
    await post.save();
    // Nếu update published
    if (typeof args.data.published === 'boolean') {
      const statePublished = args.data.published;
      if (!prePublished && statePublished) {
        // private => publish
        await pubsubPost({
          userId: user._id,
          post,
          mutation: 'CREATED',
          pubsub,
          models
        });
      } else if (prePublished && !statePublished) {
        // publish => private
        await pubsubPost({
          userId: user._id,
          post,
          mutation: 'DELETED',
          pubsub,
          models
        });
      }
    } else if (post.published) {
      await pubsubPost({
        userId: user._id,
        post,
        mutation: 'UPDATED',
        pubsub,
        models
      });
    }

    return post;
  },
  async deletePost(parent, { id }, { models, user, pubsub }, info) {
    const post = await models.Post.findOne({ _id: id, author: user._id });
    if (!post) {
      throw new Error('Unable to delete post!');
    }
    try {
      const result = await processDestroy(`images/${post.image._id}`);
      if (result.result !== 'ok') {
        throw new Error(`Image ${result.result}`);
      }

      await post.remove();
    
      if (post.published) {
        await pubsubPost({
          userId: user._id,
          post,
          mutation: 'DELETED',
          pubsub,
          models
        });
      }
    return post;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export default postMutation;