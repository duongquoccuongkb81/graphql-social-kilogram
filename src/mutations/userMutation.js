import debug from 'debug';
import { processUpload } from '../utils/processUpload';
import { sendWelcomeMail } from '../emails/account';

const logger = debug('app:userMutation');


const userMutation = {
  async createUser(parent, args, { models }, info) {
    const emailExist = await models.User.findOne({ email: args.data.email });
    if (emailExist) {
      throw new Error('This email already exists');
    }
    const usernameExist = await models.User.findOne({ username: args.data.username });
    if (usernameExist) {
      throw new Error('This username already exists');
    }
    const user = new models.User(args.data);
    user.profile = {
      name: args.name
    }
    try {
      await user.save();
      sendWelcomeMail(user.email, user.profile.name);
      const token = await user.generateAuthToken();
      return {
        token,
        user
      };
    } catch (error) {
      throw new Error(error);
    }
  },
  async uploadAvatar(parent, args, { models, user }, info) {
    const image = await processUpload(args.image);
    user.profile.avatar = image;
    await user.save();
    return user.profile.avatar;
  },
  async login(parent, { data }, { models }, info) {
    const user = await models.User.findByCredentials(data.email, data.password);
    const token = await user.generateAuthToken();
    return {
      token,
      user
    }
  },
  async updateUser(parent, { data, profileData }, { models, user }, info) {
    for (const [key, value] of Object.entries(data)) {
      user[key] = value;
    }
    for (const [key, value] of Object.entries(profileData)) {
      user.profile[key] = value;
    }
    await user.save();
    return user;
  },
  async deleteUser(parent, args, { models, user }, info) {
    try {
      await user.remove();
    } catch (error) {
      throw new Error(error);      
    }
    return user;
  },
  async follow(parent, { userId }, { models, user }, info) {
    if (userId === user._id.toString()) {
      throw new Error('Unable to follow my self');
    }
    const myFollowing = await models.Following.findOne({ userId: user._id });
    if (!myFollowing) {
      throw new Error('Following not found!');
    }
    myFollowing.following.addToSet(userId);
    const userFollowers = await models.Follower.findOne({ userId });
    if (!userFollowers) {
      throw new Error('Follower not found!');
    }
    userFollowers.followers.addToSet(user._id);
    await Promise.all([myFollowing.save(), userFollowers.save()]);
    return myFollowing.following;
  },
  async unfollow(parent, { userId }, { models, user }, info) {
    if (userId === user._id.toString()) {
      throw new Error('Unable to unfollow my self');
    }
    const myFollowing = await models.Following.findOne({ userId: user._id });
    if (!myFollowing) {
      throw new Error('Following not found!');
    }
    const userFollowers = await models.Follower.findOne({ userId });
    if (!userFollowers) {
      throw new Error('Follower not found!');
    }
    await myFollowing.unfollowing(userId);
    await userFollowers.unfollower(user._id);
    return myFollowing.following;
  },
  async logout(parent, args, { models, user, token }, info) {
    const tokenIndex = user.tokens.findIndex(el => el.token === token);
    if (tokenIndex === -1) {
      throw new Error('Unable to find token');
    }
    user.tokens.splice(tokenIndex, 1);
    await user.save();
    return user;
  },
  async logoutAll(parent, args, { models, user }, info) {
    user.tokens = [];
    await user.save();
    return user;
  }
}

export default userMutation;