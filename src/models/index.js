import User from './User';
import Post from './Post';
import Comment from './Comment';
import Favorite from './Favorite';
import Follower from './Follower';
import Following from './Following';
import Conversation from './Conversation';
import Message from './Message';


const models = {
  User,
  Post,
  Comment,
  Favorite,
  Follower,
  Following,
  Conversation,
  Message
}

export default models;