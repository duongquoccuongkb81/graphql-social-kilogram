import mongoose from 'mongoose';
import debug from 'debug';
import Conversation from './Conversation';

const logger = debug('app:MessageModel');

const messageSchema = new mongoose.Schema({
  conversation_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Conversations'
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users'
  },
  text: {
    type: String,
    required: true
  }
}, {
  timestamps: true
});

messageSchema.pre('save', async function(next) {
  const message = this;
  if (message.isNew) {
    logger('New message pre save');
    const conversation = await Conversation.findById(message.conversation_id);
    //Conversation Schema has trigger pre save to update updateAt
    await conversation.save();
  }
  next();
});

const Message = mongoose.model('Messages', messageSchema);

export default Message;