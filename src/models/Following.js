import mongoose from 'mongoose';

const followingSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
    unique: true
  },
  following: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
  }]
});

followingSchema.methods.unfollowing = async function(userId) {
  const _following = this;
  _following.following.pull(userId);
  await _following.save();
}

const Following = mongoose.model('Following', followingSchema);

export default Following;