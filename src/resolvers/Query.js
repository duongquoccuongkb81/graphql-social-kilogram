import debug from 'debug';

const logger = debug('app:QueryResolver');

const Query = {
  async users(parent, args, { models }, info) {
    const query = {};
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.query) {
      query.username = {
        $regex: args.query,
        $options: 'i'
      }
    }
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const users = await models.User.find(query).sort(sortOption).skip(skip).limit(limit);
      return users;
    }
    const users = await models.User.find(query).sort(sortOption).skip(skip).limit(limit);
    return users;
  },
  me(parent, args, { models, user }, info) {
    return user;
  },
  async myPosts(parent, args, { user }, info) {
    const opArgs = {
      path: 'posts',
      match: {}
    }
    await user.populate(opArgs).execPopulate();

    return user.posts
  },
  async posts(parent, args, { models }, info) {
    const query = { published: true }
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.query) {
      query.title = {
        $regex: args.query,
        $options: 'i'
      }
    }
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const posts = await models.Post.find(query).sort(sortOption).skip(skip).limit(limit);
      return posts;
    }
    const posts = await models.Post.find(query).sort(sortOption).skip(skip).limit(limit);
    return posts;
  },
  async comments(parent, args, { models }, info) {
    const postExists = await models.Post.findOne({ _id: args.postId, published: true});
    if (!postExists) {
      throw new Error("Unable to find post");
    }
    const query = { post: args.postId }
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const comments = await models.Comment.find(query).sort(sortOption).skip(skip).limit(limit);
      return comments;
    }
    const comments = await models.Comment.find(query).sort(sortOption).skip(skip).limit(limit);
    return comments;
  },
  async myConversations(parent, args, { models, user }, info) {
    const query = { 'participants': user._id };
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const conversations = await models.Conversation.find(query).sort(sortOption).skip(skip).limit(limit);
      return conversations;
    }
    const conversations = await models.Conversation.find(query).sort(sortOption).skip(skip).limit(limit);
    return conversations;
  },
  async conversation(parent, { id }, { models, user }, info) {
    const conversation = await models.Conversation.findOne({ _id: id, 'participants': user._id });
    if (!conversation) {
      throw new Error('Conversation not found or not have access!');
    }
    return conversation;
  },
  async followingPosts(parent, args, { models, user }, info) {
    const myFollowing = await models.Following.findOne({ userId: user._id });
    logger(JSON.stringify(myFollowing));
    const query = { 
      $or: [{
        published: true,
        author: { $in: myFollowing.following } 
      }, {
        author: user._id
      }]
    }
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.query) {
      query.title = {
        $regex: args.query,
        $options: 'i'
      }
    }
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const posts = await models.Post.find(query).sort(sortOption).skip(skip).limit(limit);
      return posts;
    }
    const posts = await models.Post.find(query).sort(sortOption).skip(skip).limit(limit);
    return posts;
  },
  async user(parent, { id }, { models }, info) {
    const user = await models.User.findById(id);
    if (!user) {
      throw new Error('User not found!');
    }
    return user;
  },
  checkLogin(parent, args, { user }, info) {
    return user._id;
  },
  async post(parent, { id }, { models,user }, info) {
    if(user) {
      const post = await models.Post.findOne({
        $or: [{
          _id: id,
          published: true
        }, {
          _id: id,
          author: user._id
        }]
      });
      if (!post) {
        throw new Error('Post not found!');
      }
      return post;
    } else {
      const post = await models.Post.findOne({ _id: id, published: true });
      if (!post) {
        throw new Error('Post not found!');
      }
      return post;
    }
    
  },
  async userFollowers(parent, { userId }, { models }, info) {
    const user = await models.User.findById(userId);
    if (!user) {
      throw new Error('User not found!');
    }
    const follower = await models.Follower.findOne({ userId: user._id });
    if (follower.followers.length <= 0) {
      return [];
    }
    const followersUser = await models.User.find({ _id: { $in: follower.followers } });
    return followersUser;
  },
  async userFollowing(parent, { userId }, { models }, info) {
    const user = await models.User.findById(userId);
    if (!user) {
      throw new Error('User not found!');
    }
    const followingOfUser = await models.Following.findOne({ userId: user._id });
    if (followingOfUser.following.length <= 0) {
      return [];
    }
    const followingUser = await models.User.find({ _id: { $in: followingOfUser.following } });
    return followingUser;
  },
  async myFavorites(parent, args, { models, user }, info) {
    const opArgs = {
      path: 'posts',
      match: {}
    }
    await user.populate(opArgs).execPopulate();
    const postIds = user.posts.map(post => post._id);
    const query = {
      $or: [{
          likedBy: user._id
      }, {
        post: { $in: postIds }
      }] 
      
    }
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const favorites = await models.Favorite.find(query).sort(sortOption).skip(skip).limit(limit);
      return favorites;
    }
    const favorites = await models.Favorite.find(query).sort(sortOption).skip(skip).limit(limit);
    return favorites;
  },
  async FavoritesByPost(parnet, args, { models }, info) {
    const post = await models.Post.findById(args.postId);
    if (!post) {
      throw new Error('Post not found');
    }
    const query = {
      post: post._id
    }
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      query._id = {
        $lt: args.after
      }
      const favorites = await models.Favorite.find(query).sort(sortOption).skip(skip).limit(limit);
      return favorites;
    }
    const favorites = await models.Favorite.find(query).sort(sortOption).skip(skip).limit(limit);
    return favorites;
  }
}

export default Query;