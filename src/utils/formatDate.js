import moment from 'moment-timezone';
import debug from 'debug'

const logger = debug('app:FormatDate');

const formatDatePost = value => {
  //value = new Date('25/05/2020');
  // const newDate = new Date(2020, 5, 21, 23, 11);
  // value = newDate;
  // logger(value);
  const timeFromNowMiliseconds = Date.now() - value;
  const momentFormat = moment(value).tz('Asia/Ho_Chi_Minh').locale('vi');
  let formated;
  
  if (timeFromNowMiliseconds < 86400000)  {
    formated = momentFormat.fromNow(true);
  } else if (timeFromNowMiliseconds >= 86400000 && timeFromNowMiliseconds < 2*86400000) {
    const yesterdayTime = new Date();
    yesterdayTime.setDate(yesterdayTime.getDate() - 1);
    
    const checkNotYesterDay = moment(value).isSame(yesterdayTime, 'day');

    logger(`Check Not YesterDay: ${checkNotYesterDay}`);
    if(checkNotYesterDay) {
      formated = momentFormat.format('[Hôm qua lúc] HH:mm');
    } else {
      formated = momentFormat.format('dddd [lúc] HH:mm');
    }
  } else if (timeFromNowMiliseconds >= 2*86400000 && timeFromNowMiliseconds < 6*86400000) {
    //formated = moment(value).locale('vi').format('dddd [lúc] HH:mm');
    formated = momentFormat.format('dddd [lúc] HH:mm');
  } else if (timeFromNowMiliseconds >= 6*86400000 && timeFromNowMiliseconds < 2629800000) {
    formated = momentFormat.format('D MMMM [lúc] HH:mm');
  } else if (timeFromNowMiliseconds >= 2629800000 && timeFromNowMiliseconds < 31557600000) {
    formated = momentFormat.format('D MMMM');
  } else if (timeFromNowMiliseconds >=31557600000) {
    formated = momentFormat.format('D MMMM YYYY');
  }

  return formated;
}

const formatDateComment = value => {
  return moment(value).tz('Asia/Ho_Chi_Minh').locale('vi').fromNow(true);
}

const formatDateConversation = value => {
  const timeFromNowMiliseconds = Date.now() - value;
  const momentFormat = moment(value).tz('Asia/Ho_Chi_Minh').locale('vi');
  let formated;
  if(timeFromNowMiliseconds <= 120000) {
    formated = momentFormat.fromNow(true);
  } else if (timeFromNowMiliseconds > 120000  && timeFromNowMiliseconds < 86400000)  {
    formated = momentFormat.format('HH:mm');
  } else if (timeFromNowMiliseconds >= 86400000 && timeFromNowMiliseconds < 6*86400000) {
    formated = momentFormat.format('ddd');
  } else if (timeFromNowMiliseconds >= 6*86400000 && timeFromNowMiliseconds < 31557600000) {
    formated = momentFormat.format('D MMM');
  } else if(timeFromNowMiliseconds > 31557600000) {
    formated = momentFormat.format('D MMM YYYY');
  }
  return formated;
}

export { formatDatePost, formatDateComment, formatDateConversation };