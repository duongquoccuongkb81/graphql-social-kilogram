const User = {
  async posts(parent, args, { models, user }, info) {
    const match = {};
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (user === null || user._id.toString() !== parent.id ) {
      match.published = true
    }
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      match._id = {
        $lt: args.after
      }
      await parent.populate({
        match,
        path: 'posts',
        options: {
          limit,
          skip,
          sort: sortOption
        }
      }).execPopulate();

      return parent.posts;
    }
    

    await parent.populate({
      match,
      path: 'posts',
      options: {
        limit,
        skip,
        sort: sortOption
      }
    }).execPopulate();

    return parent.posts;
  },
  async comments(parent, args, { models }, info) {
    const comments = await models.Comment.find({ author: parent.id });
    return comments;
  },
  async favorites(parent, args, { models, user }, info) {
    if (user !== null && user._id.toString() === parent.id) {
      const favorites = await models.Favorite.find({ likedBy: parent.id });
      return favorites;
    }
    return [];
  },
  async followers(parent, args, { models, user }, info) {
    const follower = await models.Follower.findOne({ userId: parent.id });
    if (follower.followers.length <= 0) {
      return [];
    }
    const followersUser = await models.User.find({ _id: { $in: follower.followers } });
    return followersUser;
  },
  async following(parent, args, { models, user }, info) {
    const followingOfUser = await models.Following.findOne({ userId: parent.id });
    if (followingOfUser.following.length <= 0) {
      return [];
    }
    const followingUser = await models.User.find({ _id: { $in: followingOfUser.following } });
    return followingUser;
  },
  async followed(parent, args, { models, user }, info) {
    if (user === null) {
      return 'False';
    }
    if (parent.id === user._id.toString()) {
      return 'Myself';
    }
    const myFollowing = await models.Following.findOne({ userId: user._id, 'following': parent.id });
    return myFollowing ? 'True' : 'False';
  },
  email(parent, args, { models, user }, info) {
    // console.log(user._id === parent.id);
    // console.log(typeof user._id);
    // console.log(typeof parent.id);
    if (user !== null && user._id.toString() === parent.id) {
      return user.email;
    } else {
      return null;
    }
  },
  password(parent, args, { models }, info) {
    return null;
  }
};

export { User as default }