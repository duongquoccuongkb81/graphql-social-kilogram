import userMutation from '../mutations/userMutation';
import postMutation from '../mutations/postMutation';
import commentMutation from "../mutations/commentMutation";
import favoriteMutation from '../mutations/favoriteMutation';
import messageMutation from '../mutations/messageMutation';

const Mutation = {
  createUser: userMutation.createUser,
  uploadAvatar: userMutation.uploadAvatar,
  login: userMutation.login,
  updateUser: userMutation.updateUser,
  deleteUser: userMutation.deleteUser,
  createPost: postMutation.createPost,
  updatePost: postMutation.updatePost,
  deletePost: postMutation.deletePost,
  createComment: commentMutation.createComment,
  updateComment: commentMutation.updateComment,
  deleteComment: commentMutation.deleteComment,
  favoritePost: favoriteMutation.favoritePost,
  unFavoritePost: favoriteMutation.unFavoritePost,
  follow: userMutation.follow,
  unfollow: userMutation.unfollow,
  logout: userMutation.logout,
  logoutAll: userMutation.logoutAll,
  sendMessage: messageMutation.sendMessage,
  joinTheConversation: messageMutation.joinTheConversation
}


export default Mutation;