import debug from 'debug';
import cloudinary from '../cloudinary/cloudinary';
import { formatDatePost } from '../utils/formatDate';

const logger = debug('app:PostResolver');

const Post = {
  async author(parent, args, { models }, info) {
    const user = await models.User.findById(parent.author);
    return user;
  },
  async comments(parent, args, { models }, info) {
    const match = {};
    let limit = 10;
    let skip = 0;
    const sortOption = {};
    if (args.limit) {
      limit = args.limit;
    }
    if (args.page) {
      skip = (args.page - 1) * limit;
    }
    if (args.orderBy) {
      const part = args.orderBy.split('_');
      sortOption[part[0]] = part[1] === 'ASC' ? 1 : -1;
    }
    if (args.after) {
      match._id = {
        $lt: args.after
      }
      await parent.populate({
        match,
        path: 'comments',
        options: {
          limit,
          skip,
          sort: sortOption
        }
      }).execPopulate();

      return parent.comments;
    }

    await parent.populate({
      match,
      path: 'comments',
      options: {
        limit,
        skip,
        sort: sortOption
      }
    }).execPopulate();

    return parent.comments;
  },
  async favorites(parent, args, { models }, info) {
    const favorites = await models.Favorite.find({ post: parent.id });
    return favorites;
  },
  async favorited(parent, args, { models, user }, info) {
    if (!user) {
      return false;
    }
    const isFavorited = await user.isFavorite(parent.id);
    return isFavorited;
  },
  async favoritesCount(parent, args, { models }, info) {
    const post = await models.Post.findById(parent.id);
    if (!post) {
      //post was deleted.
      return parent.favoritesCount;
    }
    await post.updateFavoriteCount();
    return post.favoritesCount;
  },
  async commentsCount(parent, args, ctx, info) {
    await parent.populate('countComments').execPopulate();
    return parent.countComments;
  },
  image(parent, args, { models }, info) {
    if (!args.transform) {
      return parent.image;
    }
    const transform = args.transform;
    if (transform.gravity) {
      transform.gravity = transform.gravity.replace('_', ':');
    }
    const path = cloudinary.v2.url(`images/${parent.image.id}`, {
      ...transform,
      quality: 100
    });
    
    const image = {
      id: parent.image.id,
      path,
      filename: parent.image.filename,
      mimetype: parent.image.mimetype,
      encoding: parent.image.encoding
    }
    
    return image;
  },
  createdAt(parent, args, info, ctx) {
    if (!args.format) {
      return parent.createdAt
    }
    const timeFormated = args.format === 'CLIENT' ? formatDatePost(parent.createdAt) : parent.createdAt;
    return timeFormated;
  }
}

export { Post as default };