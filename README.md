# graphql-social-api

## Installation

Use the package manager npm to install

```bash
npm install
```

Create folder config in root directory then create file dev.env in config folder.

```env
# dev.env file
MONGODB_URL=mongodb://127.0.0.1:27017/social-api
JWT_SECRET=cuongmap
```

## Usage

Use command to run project with dev environment

```bash
npm run dev
```

Server is running on

```bash
https://localhost:3020
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
