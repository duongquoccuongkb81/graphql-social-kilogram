import mongoose from 'mongoose';

const followerSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
    unique: true
  },
  followers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
  }]
});

followerSchema.methods.unfollower = async function(userId) {
  const follower = this;
  follower.followers.pull(userId);
  await follower.save();
}

const Follower = mongoose.model('Followers', followerSchema);

export default Follower;